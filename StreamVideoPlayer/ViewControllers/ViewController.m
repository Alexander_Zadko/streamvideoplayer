//
//  ViewController.m
//  StreamVideoPlayer
//
//  Created by Aliaksandr Zhadzko on 18.11.16.
//  Copyright © 2016 Aliaksandr Podoshva. All rights reserved.
//

#import "ViewController.h"
#import "SVPlayer.h"

@interface ViewController () <UITextFieldDelegate>
@property(weak, nonatomic) IBOutlet UITextField *port;
@property(weak, nonatomic) IBOutlet UITextField *ip;
@property(weak, nonatomic) IBOutlet UITextField *password;
@property(weak, nonatomic) IBOutlet UITextField *name;
@property(weak, nonatomic) IBOutlet UIImageView *imageView;

@property(nonatomic, strong) SVPlayer *svPlayer;

@end

@implementation ViewController {

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.port.delegate = self;
    self.ip.delegate = self;
    self.password.delegate = self;
    self.name.delegate = self;

    self.name.text = @"admin";
    self.password.text = @"8625109f1";
    self.ip.text = @"192.168.1.4";
    self.port.text = @"554";

    //NSString *testPath = @"rtsp://rtsp-v3-spbtv.msk.spbtv.com:554/spbtv_v3_1/184_110.sdp";
    NSString *testPath = @"rtsp://mpv.cdn3.bigCDN.com:554/bigCDN/definst/mp4:bigbuckbunnyiphone_400.mp4";
    self.svPlayer = [[SVPlayer alloc] initWithVideoWithPath:testPath usesTCP:NO];
    self.svPlayer.errorHandler = ^(NSError *error) {

        if (error)
            NSLog(@"ERROR domain: %@ code: %i", error.domain, error.code);

    };
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)stop:(id)sender {
    [self.svPlayer stop];
}

- (IBAction)play:(id)sender {
//    [self.svPlayer freeAll];
//    self.svPlayer = nil;
//    NSString *testPath = [NSString stringWithFormat:@"rtsp://%@:%@@%@:%@", _name.text, _password.text, _ip.text, _port.text];
//    NSLog(@"%@", testPath);
//    self.svPlayer = [[SVPlayer alloc] initWithVideoWithPath:testPath usesTCP:NO];
//    self.svPlayer.errorHandler = ^(NSError *error) {
//
//        if (error) {
//            NSLog(@"ERROR domain: %@ code: %i", error.domain, error.code);
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:error.domain message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        }
//
//    };

    __weak typeof(self) weakSelf = self;

    self.svPlayer.frameHandler = ^(UIImage *frame) {

        if (frame) {

            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.imageView.image = frame;
            });

        }

    };

    [self.svPlayer play];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


@end
