//
//  main.m
//  StreamVideoPlayer
//
//  Created by Aliaksandr Zhadzko on 18.11.16.
//  Copyright © 2016 Aliaksandr Podoshva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
