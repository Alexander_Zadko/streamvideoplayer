//
// Created by Aliaksandr Zhadzko on 18.11.16.
// Copyright (c) 2016 Aliaksandr Podoshva. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreMedia/CoreMedia.h>
#import "SVPlayer.h"
#import "avformat.h"
#import "swscale.h"
#import "imgutils.h"

typedef NS_ENUM(NSInteger, ErrorCode) {
    ErrorCode_notOpenFile,
    ErrorCode_notFindStreamInfo,
    ErrorCode_notFindVideoStream,
    ErrorCode_notFindAudioStream,
    ErrorCode_notFindCodec,
    ErrorCode_ParametersToContextFail
};

@interface SVPlayer ()

@property(nonatomic, copy) NSString *videoPath;
@property(nonatomic, assign) BOOL usesTCP;
@property(nonatomic, strong) NSThread *bgThreadStream;

@end

@implementation SVPlayer {
    int _videoStream;
    int _audioStream;

    AVFormatContext *_avFormatContext;
    AVCodecParameters *_avVideoCodecParameters;
    AVCodecParameters *_avAudioCodecParameters;
    AVCodecContext *_avVideoCodecContext;
    AVCodecContext *_avAudioCodecContext;

    AVCodec *_avVideoCodec;
    AVCodec *_avAudioCodec;
    AVFrame *_avFrame;
    AVPacket _packet;
    AVPicture _avPicture;

    struct SwsContext *_swsContext;

    uint8_t *_buffer;
    int _numBytes;

}

#pragma mark Init

- (instancetype)initWithVideoWithPath:(NSString *)videoPath usesTCP:(BOOL)usesTCP {
    if (self = [super init]) {

        self.videoPath = videoPath;
        self.usesTCP = usesTCP;

        _videoStream = -1;
        _audioStream = -1;

    }

    return self;
}

- (void)getVideoStream {
    [self setup];

    while (![[NSThread currentThread] isCancelled]) {

        if (_avFormatContext) {

            if ((av_read_frame(_avFormatContext, &_packet) >= 0)) {

                if (_packet.stream_index == _videoStream) {

                    // Decode video frame
                    int send_packet = avcodec_send_packet(_avVideoCodecContext, &_packet);
                    int receive_frame = avcodec_receive_frame(_avVideoCodecContext, _avFrame);

                    // Did we get a video frame?
                    if (send_packet == 0 && receive_frame == 0) {
                        // Convert the image from its native format to RGB
                        sws_scale(_swsContext, (const uint8_t *const *) _avFrame->data, _avFrame->linesize, 0, _avVideoCodecContext->height, _avPicture.data, _avPicture.linesize);

                        UIImage *image = [self imageFromAVPicture:_avPicture width:_avVideoCodecContext->width height:_avVideoCodecContext->height];

                        if (self.frameHandler)
                            self.frameHandler(image);


                    }

                }


            }

        }

    }

}

- (void)play {
    if (!self.bgThreadStream) {
        self.bgThreadStream = [[NSThread alloc] initWithTarget:self selector:@selector(getVideoStream) object:nil];
        self.bgThreadStream.name = @"GetVideoStreamThread";
        [self.bgThreadStream start];
    }
}

- (void)stop {
    [self.bgThreadStream cancel];
    self.bgThreadStream = nil;
    [self freeAll];
}


- (void)freeAll {

    sws_freeContext(_swsContext);

    avpicture_free(&_avPicture);
    av_packet_unref(&_packet);

    av_free(_buffer);
    av_free(_avFrame);

    avcodec_close(_avVideoCodecContext);

    // Close the video file
    avformat_close_input(&_avFormatContext);
    avcodec_free_context(&_avVideoCodecContext);

    _videoStream = -1;
    _numBytes = 0;

}

#pragma mark Private

- (void)setup {
    //register all codecs
    [self registerAVCodecs];
    [self openStream];

    BOOL decode = [self decodeVideoStream] && [self decodeAudioStream];
    BOOL streams = _videoStream > -1 && _audioStream > -1;
    if (streams && decode) {

        // Allocate video frame
        _avFrame = av_frame_alloc();

        // Determine required buffer size and allocate buffer
        _numBytes = av_image_get_buffer_size(_avVideoCodecContext->pix_fmt, _avVideoCodecContext->width, _avVideoCodecContext->height, 1);
        _buffer = (uint8_t *) av_malloc(_numBytes * sizeof(uint8_t));

        NSLog(@"Determine required buffer size and allocate buffer");
        NSLog(@"video size w:%i, h:%i", _avVideoCodecContext->width, _avVideoCodecContext->height);
        NSLog(@"numBytes size: %i", _numBytes);

        [self setupSWSContext];

    } else {

        NSLog(@"ERROR");

    }

}

- (void)registerAVCodecs {
    avcodec_register_all();
    av_register_all();
}

- (void)openStream {

    // Set the RTSP Options
    AVDictionary *opts = 0;
    if (self.usesTCP)
        av_dict_set(&opts, "rtsp_transport", "tcp", 0);

    //open video file
    avformat_network_init();

    if (avformat_open_input(&_avFormatContext, [self.videoPath UTF8String], NULL, &opts) != 0) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't open file\n");
        self.errorHandler([NSError errorWithDomain:@"Couldn't open file" code:ErrorCode_notOpenFile userInfo:nil]);
    }

    if (avformat_find_stream_info(_avFormatContext, NULL) < 0) {
        av_log(NULL, AV_LOG_ERROR, "Couldn't find stream information\n");
        self.errorHandler([NSError errorWithDomain:@"Couldn't find stream information" code:ErrorCode_notFindStreamInfo userInfo:nil]);
    }

    // Find the first video stream
    _videoStream = -1;

    for (int i = 0; i < _avFormatContext->nb_streams; i++) {

        if (_avFormatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            _videoStream = i;
            NSLog(@"video stream did find");
        }

        if (_avFormatContext->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
            _audioStream = i;
            NSLog(@"audio stream did find");
        }

    }

    if (_videoStream == -1) {
        NSLog(@"Didn't find a video stream");
        self.errorHandler([NSError errorWithDomain:@"Didn't find a video stream" code:ErrorCode_notFindVideoStream userInfo:nil]);
    }

    if (_audioStream == -1) {
        NSLog(@"Didn't find a video stream");
        self.errorHandler([NSError errorWithDomain:@"Didn't find a audio stream" code:ErrorCode_notFindAudioStream userInfo:nil]);
    }

}

#pragma mark DecodeStreams

- (BOOL)decodeVideoStream {

    if (_videoStream > 0) {

        // Get a pointer to the codec context for the video stream
        _avVideoCodecParameters = _avFormatContext->streams[_videoStream]->codecpar;

        // Find the decoder for the video stream
        _avVideoCodec = avcodec_find_decoder(_avVideoCodecParameters->codec_id);

        if (_avVideoCodec == NULL) {
            self.errorHandler([NSError errorWithDomain:@"can not find codec for codec id" code:ErrorCode_notFindCodec userInfo:@{@"codec_id": [NSValue value:&_avVideoCodecParameters->codec_id withObjCType:@encode(enum AVCodecID)]}]);
            return NO;
        }

        // Open codec
        _avVideoCodecContext = avcodec_alloc_context3(_avVideoCodec);
        if (avcodec_parameters_to_context(_avVideoCodecContext, _avVideoCodecParameters) >= 0) {

            if (avcodec_open2(_avVideoCodecContext, _avVideoCodec, NULL) < 0) {
                self.errorHandler([NSError errorWithDomain:@"Could not open codec" code:ErrorCode_notFindCodec userInfo:@{@"AVCodec": [NSValue valueWithPointer:_avVideoCodec]}]);
                return NO;

            } else {

                NSLog(@"Video codec is open");

            }

        } else {

            self.errorHandler([NSError errorWithDomain:@"avcodec_parameters_to_context is fail" code:ErrorCode_ParametersToContextFail userInfo:@{@"AVCodecParametersOrig": [NSValue valueWithPointer:_avVideoCodecParameters]}]);
            return NO;

        }

    }

    return YES;
}

- (BOOL)decodeAudioStream {

    if (_audioStream >= 0) {

        // Get a pointer to the codec context for the audio stream
        _avAudioCodecParameters = _avFormatContext->streams[_audioStream]->codecpar;

        // Find the decoder for the audio stream
        _avAudioCodec = avcodec_find_decoder(_avAudioCodecParameters->codec_id);

        if (_avAudioCodec == NULL) {
            self.errorHandler([NSError errorWithDomain:@"can not find codec for codec id" code:ErrorCode_notFindCodec userInfo:@{@"codec_id": [NSValue value:&_avAudioCodecParameters->codec_id withObjCType:@encode(enum AVCodecID)]}]);
            return NO;
        }

        // Open audio codec

        _avAudioCodecContext = avcodec_alloc_context3(_avAudioCodec);

        if (avcodec_parameters_to_context(_avAudioCodecContext, _avAudioCodecParameters) >= 0) {

            if (avcodec_open2(_avAudioCodecContext, _avAudioCodec, NULL) < 0) {
                self.errorHandler([NSError errorWithDomain:@"Could not open codec" code:ErrorCode_notFindCodec userInfo:@{@"AVCodec": [NSValue valueWithPointer:_avAudioCodec]}]);
                return NO;

            } else {

                NSLog(@"Audio codec is open");

            }

        } else {

            self.errorHandler([NSError errorWithDomain:@"avcodec_parameters_to_context is fail" code:ErrorCode_ParametersToContextFail userInfo:@{@"AVCodecParametersOrig": [NSValue valueWithPointer:_avAudioCodecParameters]}]);
            return NO;

        }


    }

    return YES;
}

#pragma mark Allocate RGB picture

- (void)setupSWSContext {

    // Assign appropriate parts of buffer to image planes in pFrameRGB
    // Note that pFrameRGB is an AVFrame, but AVFrame is a superset

    avpicture_alloc(&_avPicture, AV_PIX_FMT_RGB24, _avVideoCodecContext->width, _avVideoCodecContext->height);

    // Allocate RGB picture

    _swsContext = sws_getContext(
            _avVideoCodecContext->width,
            _avVideoCodecContext->height,
            _avVideoCodecContext->pix_fmt,
            _avVideoCodecContext->width,
            _avVideoCodecContext->height,
            AV_PIX_FMT_RGB24,
            SWS_BILINEAR, NULL, NULL, NULL);

}

#pragma mark Convert to image

- (UIImage *)imageFromAVPicture:(AVPicture)pict width:(int)width height:(int)height {
    CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
    CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault, pict.data[0], pict.linesize[0] * height, kCFAllocatorNull);
    CGDataProviderRef provider = CGDataProviderCreateWithCFData(data);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGImageRef cgImage = CGImageCreate(width,
            height,
            8,
            24,
            pict.linesize[0],
            colorSpace,
            bitmapInfo,
            provider,
            NULL,
            NO,
            kCGRenderingIntentDefault);
    CGColorSpaceRelease(colorSpace);
    UIImage *image = [UIImage imageWithCGImage:cgImage];

    CGImageRelease(cgImage);
    CGDataProviderRelease(provider);
    CFRelease(data);

    return image;
}

@end
