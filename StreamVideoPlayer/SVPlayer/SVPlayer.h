//
// Created by Aliaksandr Zhadzko on 18.11.16.
// Copyright (c) 2016 Aliaksandr Podoshva. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SVPlayer : NSObject

@property (nonatomic, copy) void (^errorHandler)(NSError *error);
@property (nonatomic, copy) void (^frameHandler)(UIImage *frame);

- (instancetype)initWithVideoWithPath:(NSString *)videoPath usesTCP:(BOOL)usesTCP;

- (void)play;

- (void)stop;

@end